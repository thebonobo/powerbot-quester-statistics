from flask import Flask, request, abort, Response
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from flask_restful import Resource, Api
from functools import wraps
import os
import sys
import string
import urllib.request
import urllib.parse
from flaskext.mysql import MySQL
from geolite2 import geolite2

app = Flask(__name__)
mysql = MySQL()
# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'th6k5gfkgf54'
app.config['MYSQL_DATABASE_DB'] = 'statistics'
app.config['MYSQL_DATABASE_HOST'] = 'runescape-statistics-db'
app.config['MYSQL_DATABASE_PORT'] = 3306
app.config['MYSQL_DATABASE_CHARSET'] = 'latin1'
mysql.init_app(app)
api = Api(app)
limiter = Limiter(
    app,
    key_func=get_remote_address,
    default_limits=["15 per hour"]
)

@app.route('/send_quest_statistic', methods=["GET"])
@limiter.limit("2/minute")
def update_quest_statistic():
    if (len(request.args) is not 6):
        abort(400)
    
    quest = request.args.get('quest')
    session_time =  request.args.get('session_time')
    total_quest_points = request.args.get('total_quest_points')
    member = request.args.get('member')
    ironman = request.args.get('ironman')
    machine_string = request.args.get('machine_string')
    ip = request.remote_addr
    #ip = "50.50.50.50"
    
    conn = None 
    cursor = None
    reader = None
    
    try:
        reader = geolite2.reader()
        ip_info = reader.get(ip)
        geolite2.close()
        if (ip_info == None):
            country = "None"
        else:
            country = ip_info.country.iso_code

        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.callproc('sp_insertStatistic',(quest, total_quest_points, session_time, member, ironman, country, machine_string))
        print("Quest" + quest + " completed in " + str(session_time), file=sys.stderr)
        conn.commit()
    except Exception as e:
        print(repr(e), file=sys.stderr)
        return Response(
            'Our database is not available at this time.', 503, mimetype='application/json')
    finally:
        if not cursor == None:
            cursor.close() 
        if not conn ==  None:
            conn.close()
   
    return "Statistics have been received"

if __name__ == '__main__':
     app.run(host='0.0.0.0')
