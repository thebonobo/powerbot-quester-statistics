SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE DATABASE IF NOT EXISTS `statistics` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `statistics`;

CREATE OR REPLACE TABLE `statistics`.`quests` (
     `quest_id` SMALLINT unsigned NOT NULL AUTO_INCREMENT ,
     `quest_name` VARCHAR(50) NOT NULL ,
     `quest_points` TINYINT unsigned NOT NULL ,
    PRIMARY KEY (`quest_id`)) ENGINE = InnoDB DEFAULT CHARSET=latin1;
    CREATE UNIQUE INDEX quest_id ON quests(quest_id);  -- Create unique index on foo.

INSERT INTO `statistics`.`quests`
    (`quest_id`,
    `quest_name`,
    `quest_points`)
VALUES
    (29, "Cooks Assistant", 1),
    (31, "Ernest The Chicken", 4),
    (62, "Goblin Diplomacy",5),
    (63, "Rune Mysteries",1),
    (67, "Witch's Potion", 1),
    (71, "Pirates Treasure", 2),
    (107, "The Restless Ghost", 1),
    (122, "The Knights Sword", 1),
    (144, "Romeo and Juliet", 5),
    (160,"Imp Catcher", 1),
    (179, "Sheep Shearer", 1),
    (222, "Demon Slayer", 3),
    (273, "Prince Ali Rescue", 3),
    (1677, "The Corsair Curse",2);

CREATE OR REPLACE TABLE `statistics`.`quest_completions` (
     `quest_completion_id` INT unsigned NOT NULL AUTO_INCREMENT ,
     `quest_id` SMALLINT unsigned NOT NULL ,
     `quest_date` DATETIME NOT NULL , 
     `quest_length` SMALLINT unsigned NOT NULL ,
     `total_quest_points` SMALLINT unsigned NOT NULL ,
     `member` BOOLEAN NOT NULL ,
     `ironman` BOOLEAN NOT NULL ,
     `country` VARCHAR(48) NOT NULL ,
     `machine_string` VARCHAR(18) NOT NULL,
    PRIMARY KEY (`quest_completion_id`),
    FOREIGN KEY (quest_id) REFERENCES quests(quest_id)) ENGINE = InnoDB DEFAULT CHARSET=latin1;

    DELIMITER $$
    CREATE OR REPLACE PROCEDURE `statistics`.`sp_insertStatistic`(
    IN p_quest_id SMALLINT,
    IN p_total_quest_points SMALLINT,
    IN p_quest_length SMALLINT,
    IN p_member BOOLEAN,
    IN p_ironman BOOLEAN,
    IN p_country VARCHAR(48),
    IN p_machine_string VARCHAR(18)
    )
    BEGIN
        insert into quest_completions 
        (   
            quest_id,
            quest_date,
            quest_length,
            total_quest_points,
            member,
            ironman,
            country,
            machine_string
        )
        values
        (
            p_quest_id,
            NOW(),
            p_quest_length,
            p_total_quest_points,
            p_member,
            p_ironman,
            p_country,
            p_machine_string
        );
    END$$

DELIMITER ;

